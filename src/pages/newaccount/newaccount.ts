import { Component } from '@angular/core';

import { NavController,NavParams,LoadingController,AlertController } from 'ionic-angular';
import { SfdcService } from "../../providers/sfdc-service";
import { SfdcData } from "../../providers/sfdc-data";

declare var cordova : any;
declare var navigator : any;
declare var Connection: any;
@Component({
  selector: 'newaccount',
  templateUrl: 'newaccount.html'
})
export class NewAccount {
  
  account : any;
  loading : any;
  ratingV : Array<String>;
  industry : Array<String>;
  typeV : Array<String>;
	mSmartSync : any;
  mSmartStore : any;


  constructor(public alertCtrl: AlertController,public navCtrl: NavController,public service: SfdcService,public dataService: SfdcData,public navParams: NavParams,public loadingCtrl: LoadingController) {
    this.account = this.navParams.data;
    if(this.account == undefined)
      this.account = { Name : '',
                       Type : '',
                       Rating : '',
                       Industry : '',
                       Description :'',
                       BillingStreet : '',
                       BillingCity : '',
                       BillingState :'',
                       BillingCountry :''
                     };
    
    this.typeV = ["Prospect"," Customer - Direct","Customer - Channel","Channel Partner / Reseller","Installation Partner","Technology Partner","Other"];

    this.ratingV = ["Hot","Warm","Cold"];
    this.industry = ["Agriculture","Apparel","Banking","Biotechnology","Chemicals","Communications","Construction","Consulting","Education","Electronics","Energy","Engineering","Entertainment"];


  }

  saveAccount(event){
  	// let loader = this.loadingCtrl.create({
   //                content: 'Please wait...getting contacts',
   //                dismissOnPageChange: true
   //  })
   //  loader.present();

   //   this.service.createAccount(this.account).then(data => {
  	// 	  console.log("RESP" + JSON.stringify(data));
   //      let acc = { Id : data.id , Name : this.account.Name , Rating : this.account.Rating };
   //      this.dataService.pushNewAccount(acc);
   //      // Navigate to list view
  	// 	  this.navCtrl.popToRoot();

   //   }).catch(error => console.log("ERROR" + JSON.stringify(error)));
   console.log(this.account);
    this.account.__locally_updated__ = true;
    this.account.__local__ = true;
    this.getSmartStore().upsertSoupEntries(
      {storeName: "Mobcap", isGlobalStore:false},
      "Account", 
      [ this.account ],
      accounts => {
        alert("Registro salvo com sucesso!");
      },
      exception => {
        alert(exception);
      } 
    ); 
  }

  doSyncUp() {
    if(this.isOnline()){
      let target = {
        createFieldlist: ["Name", "Rating", "Type"], 
        updateFieldlist: ["Name", "Rating", "Type"]
      };
      this.getSmartSync().syncUp(
        {storeName: "Mobcap", isGlobalStore:false},
        target,
        "Account",
        {mergeMode:"OVERWRITE", attributes: {type:"Account"}},
        callbackSuccess => {
          console.log(callbackSuccess);
        }
      );  
    }    
  }

  isOnline() {
    let result = true;
    let mCurrentConnection = navigator.connection.type;
    console.log("Current Connection: " + mCurrentConnection);
    if(mCurrentConnection == Connection.NONE || mCurrentConnection == Connection.UNKNOWN){
      result = true;
    }
    return result;
  }

  getSmartStore() {
    if(this.mSmartStore == undefined)
      this.mSmartStore = navigator.smartstore;
    return this.mSmartStore;
  }

  getSmartSync() {
    if(this.mSmartSync == undefined)
      this.mSmartSync = cordova.require("com.salesforce.plugin.smartsync");
    return this.mSmartSync;
  }


}

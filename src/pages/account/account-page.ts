import { Component } from '@angular/core';

import {NavController,NavParams,LoadingController} from 'ionic-angular';
import { SfdcService } from "../../providers/sfdc-service";
import { SfdcData } from "../../providers/sfdc-data";
import { AccountDetail } from '../accountdetail/accountdetail';
import { NewAccount } from '../newaccount/newaccount';

declare var cordova : any;
declare var navigator : any;
declare var Connection: any;
@Component({
  selector: 'account-page',
  templateUrl: 'account-page.html'
})
export class Accountlist {

  accountsList : Array<any> ;
  loading : any;
  accountRec : any;
  mSmartSync : any;
  mSmartStore : any;

  constructor(public navCtrl: NavController,public navParams: NavParams,public service: SfdcService,public dataService: SfdcData,public loadingCtrl: LoadingController) {
    this.accountsList = [];
    this.getAllAccount();
    this.doSyncAccount();
    document.addEventListener("sync", this.onProgressSync.bind(this));
    setInterval(pCallbackInterval => {
        this.doSyncUp();
    }, 3000);
  }

  onSuccessRegisterSoap(pSuccessSoapEntity) {
    console.log(pSuccessSoapEntity);
    console.log("Success Create Table Account... ");
    this.loadAccount();
  }

  loadAccount() {
    if(this.getLoadingDialog()._isHidden != true){
      this.getLoadingDialog().present();  
      console.log(this.getLoadingDialog());
      console.log(this.getLoadingDialog());
      console.log(this.getSmartSync());
      // let querySpec = navigator.smartstore.buildSmartQuerySpec("SELECT {Account:Id}, {Account:Name} FROM {Account}", 1);
      let querySpec = this.getSmartStore().buildAllQuerySpec(null, null, 200);
      // navigator.smartstore.runSmartQuery(
      //   {storeName: "Mobcap", isGlobalStore:false},
      //   querySpec, 
      //   function(cursor) { 
      //     console.log(cursor);  
      //     // that.accountsList = 
      //   },
      //   function(pErrorEntity) { 
      //     console.log(pErrorEntity);  
      //   }
      // );
      this.accountsList = [];
      this.getSmartStore().querySoup(
        {storeName: "Mobcap", isGlobalStore:false},
        "Account", 
        querySpec, 
        this.onProgressQuery.bind(this),
        this.onErrorQuery
      );  
    }
    
  }

  doSyncAccount(){
    if(this.isOnline()){
      console.log("Start Download Account... ");
      this.getSmartSync().syncDown(
        {storeName: "Mobcap", isGlobalStore:false},
        {type:"soql", query:"SELECT Id, Name, Rating, Type FROM Account"}, 
        "Account", 
        {mergeMode:"LEAVE_IF_CHANGED"}, 
        this.onStartSyncDown.bind(this),
        this.onErrorSyncDown.bind(this)
      );  
    }
  }

  isOnline() {
    let result = true;
    let mCurrentConnection = navigator.connection.type;
    console.log("Current Connection: " + mCurrentConnection);
    if(mCurrentConnection == Connection.NONE || mCurrentConnection == Connection.UNKNOWN){
      result = false;
    }
    return result;
  }

  onErrorRegisterSoap(pErrorEntity) {
    console.log(pErrorEntity);
  }

  onStartSyncDown(pEventEntity) {
    
  }

  doSyncUp() {
    if(this.isOnline()){
      let target = {
        createFieldlist: ["Name", "Rating", "Type"], 
        updateFieldlist: ["Name", "Rating", "Type"]
      };
      this.getSmartSync().syncUp(
        {storeName: "Mobcap", isGlobalStore:false},
        target,
        "Account",
        {mergeMode:"OVERWRITE", attributes: {type:"Account"}},
        callbackSuccess => {
          
        }
      );
    }
  }

  onProgressSync(pEvent) {
    if(pEvent.detail.status == "DONE" && pEvent.detail.totalSize > 0){
      console.log("DONE SYNC TYPE: " + pEvent.detail.type);
      if(pEvent.detail.type == "syncDown")
        this.loadAccount();
    }
  }

  onErrorQuery(pErrorEntity) {
    console.log(pErrorEntity);  
  }

  onProgressQuery(cursor) {
      console.log(cursor);
      for(let i = 0; i < cursor.currentPageOrderedEntries.length; i++){
        this.accountsList.push(cursor.currentPageOrderedEntries[i]);
      }

      this.getSmartStore().moveCursorToNextPage(
        {storeName: "Mobcap", isGlobalStore:false},
        cursor, 
        nextCursor => {
          console.log("Proxima etapa");
          console.log(nextCursor);
          console.log(cursor);
          this.onProgressQuery(nextCursor);
        }, 
        exception => {
          console.log(exception);
          this.getSmartStore().closeCursor(
            {storeName: "Mobcap", isGlobalStore:false},
            cursor, 
            this.onFinishQuery.bind(this)
          );
        }
      );
  }

  onFinishQuery() {
    this.getLoadingDialog().dismiss();
  }

  onErrorSyncDown(pErrorEntity) {
    console.log(pErrorEntity);
  }

  getAllAccount() { 
    let mIndexSpec = this.getSoapSpecs();
    this.getSmartStore().registerSoup(
      {storeName: "Mobcap", isGlobalStore:false}, 
      "Account", 
      mIndexSpec, 
      this.onSuccessRegisterSoap.bind(this), 
      this.onErrorRegisterSoap.bind(this)
    );                          
  }

  getSmartStore() {
    if(this.mSmartStore == undefined)
      this.mSmartStore = navigator.smartstore;
    return this.mSmartStore;
  }

  getSmartSync() {
    if(this.mSmartSync == undefined)
      this.mSmartSync = cordova.require("com.salesforce.plugin.smartsync");
    return this.mSmartSync;
  }

  getLoadingDialog() {
    if(this.loading == undefined)
      this.loading = this.loadingCtrl.create({
        content: 'Carregando contas...',
        dismissOnPageChange: true
      });
    return this.loading;
  }

  getSoapSpecs() {
    return [
      {path:"Id",type:"string"},
      {path:"Name",type:"string"},
      {path:"Rating",type:"string"},    
      {path:"Type",type:"string"},      
      {path:"LastModifiedDate",type:"string"},
      {path:"__locally_created__",type:"boolean"},
      {path:"__locally_updated__",type:"boolean"},
      {path:"__locally_deleted__",type:"boolean"},
      {path:"__local__",type:"boolean"}
    ];
  }

  itemTapped(event, account) {

    // let loader = this.loadingCtrl.create({
    //               content: 'Carregando conta...',
    //               dismissOnPageChange: true
    // })
    // loader.present();

    // this.service.getAccountById(account.Id).then(data => {
    //       delete data.attributes;
    //             loader.dismiss();
    //             this.accountRec = data;               
    //             this.navCtrl.push(AccountDetail,this.accountRec);
    // }).catch(error => console.log("ERROR" + JSON.stringify(error)));

    
    // this.accountRec = data;               
    // this.navCtrl.push(AccountDetail,this.accountRec);
    this.navCtrl.push(NewAccount,account);
    
  }

  createAccount(event){
      this.navCtrl.push(NewAccount);
  }

}
